module.exports = (response) => {
    let speechOutput = "";
    // Handling unsuccessful response from API
    if (!response) {
        speechOutput = "An unknown error occured. Please contact our support.\nError: " + response.error;
    } else if (response.success != true) {
        if (response.statusCode == 400) {
        //Handle API errors that come with their own error messages
        //This basically just wraps existing messages in a more readable format for the Alexa
        if (response.error.toLowerCase().startsWith("unable")) {
            if (response.error.toLowerCase().includes("IATA")) {
            //Format "Unable to find the airports. Please use IATA airport codes only"
            speechOutput = "I couldn't find that airport. Please only give me IATA codes.";
            } else {
            //Format "Unable to find <emission type> for <item type> in <region>"
            speechOutput = "I was " + response.error + ". Please try again.";
            }
        } else if (response.error.toLowerCase().startsWith("please provide")) {
            //Format "Please provide valid sector and region values"
            speechOutput = "Sorry, I'm missing some info from you. " + response.error + ".";
        } else if (response.error.toLowerCase().includes("cannot be less than zero")) {
            //Format "Distance cannot be less than zero"
            speechOutput = "Sorry, I can't use a negative distance or mileage. Please try again.";
        } else {
            speechOutput = "An unknown error occured. Please report this to the developers.\nError: " + response.error;
        }
        } else if (response.statusCode == 403 || response.statusCode == 406) {
        // Forbidden, not acceptable
        speechOutput = "An unknown error occured. Please report this to the developers.\nError: " + response.error;
        } else if (response.statusCode == 404) {
        // Not found
        speechOutput = "The data you requested isn't available. Please try again";
        } else if (response.statusCode == 429) {
        // Too many requests
        speechOutput = "I'm feeling a bit overwhelmed right now. Try asking me again later.";
        } else if (response.statusCode == 500) {
        // Internal server error
        speechOutput = "There's a problem with our server. Please try again in a bit.";
        } else if (response.statusCode == 503) {
        // Service unavailable
        speechOutput = "The server is currently offline for maintenance. Please try again later.";
        } else {
        speechOutput = "An unknown error occured. Please contact our support.\nError: " + response.error;
        }
    }

    return speechOutput;
}