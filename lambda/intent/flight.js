const config = require('../config');
const FLIGHT_ENDPOINT = config.baseUrl + "/flight";

let request = require("request");
let apiresponse = require('../utils');

// Calling the API to receive the response
let callEmissionsApi = function (options) {
    return new Promise(function (resolve, reject) {
        request(options, function (error, response, body) {
            resolve(body)
        })
    })
}

// @TODO Add Error checking
const getResolvedValue = (value) => {
    var resolutions = value.resolutions.resolutionsPerAuthority;
    var value = "";
    if (resolutions.length > 0) {
        var values = resolutions[0].values;
        if (values.length > 0) {
            value = values[0].value.name
        }
    }
    return value;
}

// Validate the slot if not a valid input then repromt user to give a valid input
let getBadSlots = (slots) => {
  return new Promise((resolve, reject) => {
    let badInputSlots;
    for (let x in slots) {
      if(slots.hasOwnProperty(x) 
        && (x == 'destination' || x == 'origin' )
        && slots[x].hasOwnProperty('resolutions')) {

        var tempX = slots[x]['resolutions']['resolutionsPerAuthority'][0];
            
        if(tempX.hasOwnProperty('status')
          && tempX.status.hasOwnProperty('code')) {
          
            if (tempX.status.code === 'ER_SUCCESS_NO_MATCH') {
              badInputSlots = x;
              break;
            }
        }
      }
    }
    resolve(badInputSlots);
  });
}

// Generate the response according to the user input parameter
let responseGen = (response, newParams) => {
    // Handle Api Error
    let speechOutput = apiresponse(response);

    if (response.success == true && speechOutput === "") {
      speechOutput = `Carbon Dioxide emitted by ${newParams.flightType} flight ${newParams.model} from ${newParams.originValue} to ${newParams.destinationValue} `;
      if (newParams.passenger != "") {
        speechOutput += `with ${newParams.passenger} passengers `
      }

        speechOutput += `is ${response.emissions.CO2.toFixed(2)} ${response.unit}`;
    }

    return speechOutput;
}

/*
  Handle the Flight Intent and parse the JSON to fetch the required field
  and generate a response according that.
*/
const flight_intent = {
    canHandle(handlerInput) {
      const request = handlerInput.requestEnvelope.request;
      return request.type === 'IntentRequest'
        && request.intent.name === 'flight_intent';
    },
    async handle(handlerInput) {
      
      let newParams = {};
      let passengers;
      let origin;
      let destination;
      let flightType;
      let model;
  
      const currentIntent = handlerInput.requestEnvelope.request.intent;
      // Getting values of slots and also handling in case of errors
      let slots = currentIntent.slots;
      let badInputSlots = await getBadSlots(slots);

      if (badInputSlots) {

        let speechFill;
        if(badInputSlots == 'destination') {
          speechFill = 'Destination';
        } else if (badInputSlots == 'origin') {
          speechFill = 'Origin';
        }

        return handlerInput.responseBuilder
          .speak(`I did not understand. Please respond with a valid ${speechFill}.`)
          .reprompt('Please respond with a valid Airport')
          .addElicitSlotDirective(badInputSlots, currentIntent)
          .getResponse();
      } else {

        origin = getResolvedValue(handlerInput.requestEnvelope.request.intent.slots.origin);
        destination = getResolvedValue(handlerInput.requestEnvelope.request.intent.slots.destination);

        try {
          flightType = handlerInput.requestEnvelope.request.intent.slots.flightType.value;
        } catch (error) {
          console.log(error);
        }

        try {
          model = handlerInput.requestEnvelope.request.intent.slots.model.value;
        } catch (error) {
          console.log(error);
        }

        try {
          passengers = handlerInput.requestEnvelope.request.intent.slots.passangers.value;
        } catch (error) {
          console.log(error);
        }


        // Assigning values to newParams and setting default values in case slot returns undefined
        if (origin != undefined && origin !== "") {
            newParams.origin = origin;
            newParams.originValue = handlerInput.requestEnvelope.request.intent.slots.origin.value;
        }

        if (destination != undefined && destination !== "") {
            newParams.destination = destination;
            newParams.destinationValue = handlerInput.requestEnvelope.request.intent.slots.destination.value;
        }

        if (model != undefined && model != "") {
          newParams.model = model;
        } else {
          newParams.model = "";
        }

        if (flightType !== undefined && flightType != "") {
          newParams.flightType = flightType;
        } else {
          newParams.flightType = "";
        }

        if (passengers != undefined && passengers !== "") {
            newParams.passenger = passengers;
        } else {
          newParams.passenger = "";
        }  

        // Setting up options to send request to API
        let options = {
          method: 'POST',
          url: FLIGHT_ENDPOINT,
          headers: {
            'cache-control': 'no-cache',
            'access-key': config.apiKey,
            'Content-Type': 'application/json'
          },
          body: {
            origin: newParams.origin,
            destination: newParams.destination,
            passengers: newParams.passenger,
            type: newParams.flightType,
            model: newParams.model
          },
          json: true
        };

      // Receiving response from API
          let response = await callEmissionsApi(options);
          let speechOutput = responseGen(response, newParams);

          console.log("speechOutput ->", speechOutput);

        return handlerInput.responseBuilder
          .speak(speechOutput)
          .reprompt(speechOutput)
          .getResponse();
      }
    }
  };

  module.exports = flight_intent;