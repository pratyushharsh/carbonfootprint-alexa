"use strict";
const grunt = require("grunt");
grunt.loadNpmTasks("grunt-aws-lambda");

grunt.initConfig({
    lambda_invoke: {
        LaunchTest: {
            options: {
                event: "test/event.json"
            }
        },
        FlightTest: {
            options: {
                event: "test/event2.json"
            }
        }
    },
    lambda_deploy: {
        default: {
            arn: 'arn:aws:lambda:us-east-1:466647241023:function:carbonFootprintFunction',
            options: {

            }
        }
    },
    lambda_package: {
        default: {
            options: {
                include_time: false,
                include_version: false
            }
        }
    }
});

grunt.registerTask("build", ["lambda_package", "lambda_deploy"])
grunt.registerTask("invoke", "lambda_invoke");